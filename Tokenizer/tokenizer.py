# Reg Ex needed for splitting with multiple delimeters
import re

def tokenize(f_name):
	code = []

	reading = True
	with open(f_name, 'r') as source:
		for a_line in source:
			# clear the variable for every line
			code_line = ''
			for i, a_char in enumerate(a_line):
				# ignore if multiline comment started
				if a_line[i] == '/' and a_line[i+1] == '*':
						reading = False
				
				if reading is True:
					# When encountered comment, stop reading
					if a_line[i] == '/' and a_line[i+1] == '/':
						 break
					code_line += a_line[i]
				
				if reading is False:
					if a_line[i-1] == '*' and a_line[i] == '/':
						reading = True
			for x in re.split(' |\\t|\\n',code_line):
				code.append(x)

	# remove empty spaces
	code = filter(None, code)
	
	#final splits
	new_elemnts = []
	split_code = []
	for i, element in enumerate(code):
		new_elemnts = re.split('(\.|\(|\)|\;|\=|\}|\{|\>|\<|\"|\&)', element)
		# remove empty spaces
		new_elemnts = filter(None, new_elemnts)
		
		#create the final split list
		for token in new_elemnts:
			split_code.append(token)
	
	# generate the XML file
	stringRead = False
	stringConstant = ''
	with open(f_name.replace('.jack','.xml'),'w') as out_file:
		out_file.write('<tokens>')
		for token in split_code:
			if token in ['"', '\'']:
				if stringRead is False:
					stringRead = True
					stringConstant = stringConstant + token
				else:
					out_file.write('<stringConstant>' + stringConstant + '</stringConstant>')
					stringConstant = ''
					stringRead = False
			elif stringRead is True:
				stringConstant = stringConstant + token
			elif token in [',', '.', '/', '<', '>', '?', ';', ':', '\'', '[', ']', '{', '}', '|', '\\', '!', '@', '#', '$', '%', '^', '&', '*', '(', ')', '-', '+', '='] :
				out_file.write('<symbol>' + token + '</symbol>')
			elif token in ['if', 'for', 'class', 'let', 'function', 'void', 'do', 'return', 'var', 'this', 'method', 'field', 'while'] :
				out_file.write('<keyword>' + token + '</keyword>')
			else:
				if token.isdigit():
					out_file.write('<integerConstant>' + token + '</integerConstant>')
				else:
					out_file.write('<identifier>' + token + '</identifier>')
		out_file.write('</tokens>')
		
	print split_code
	
	#Wait for input (Just for pause)
	raw_input()
	
tokenize("Main.jack")